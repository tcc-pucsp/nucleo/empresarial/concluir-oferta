package projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
@SpringBootTest
class ConcluirOfertaApplicationTests {

	@Test
	void contextLoads() {
	}

}
