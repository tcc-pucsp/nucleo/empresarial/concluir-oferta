package projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.modelo.Oferta;

public interface OfertaRepositorio extends JpaRepository<Oferta, Integer> {

    @Modifying
    @Transactional
    @Query("update Oferta set situacaoOferta = :situacaoOferta, quantidadeAlimento = quantidadeAlimento - :quantidadeAlimento " +
            "where situacaoOferta <> 2 and id = :id and " +
            "(quantidadeAlimento - :quantidadeAlimento) >= 0")
    Integer atualizarOferta(
            @Param("id") Integer id,
            @Param("situacaoOferta") Integer situacaoOferta,
            @Param("quantidadeAlimento") Integer quantidadeAlimento);

}
