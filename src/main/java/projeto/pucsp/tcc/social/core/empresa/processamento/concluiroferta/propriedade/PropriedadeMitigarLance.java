package projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.propriedade;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

@ToString
@Setter
@Getter
@ConfigurationProperties(prefix = "rabbit.mitigar.lance")
@Validated
public class PropriedadeMitigarLance {

    private String topicExchange;

}
