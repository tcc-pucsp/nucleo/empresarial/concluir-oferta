package projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.modelo.Transacao;

public interface TransacaoRepositorio extends JpaRepository<Transacao, Integer> {

}
