package projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.propriedade.PropriedadeConcluirOferta;
import projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.propriedade.PropriedadeMitigarLance;

@EnableConfigurationProperties({PropriedadeMitigarLance.class, PropriedadeConcluirOferta.class})
@SpringBootApplication
public class ConcluirOfertaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConcluirOfertaApplication.class, args);
	}

}
