package projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.cliente.implementacao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;
import projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.cliente.LanceRecurso;
import projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.modelo.Lance;
import projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.propriedade.PropriedadeMitigarLance;

@Slf4j
@Component
public class LanceRecursoImpl implements LanceRecurso {

    private final RabbitTemplate rabbitTemplate;

    private final PropriedadeMitigarLance propriedadeMitigarLance;

    public LanceRecursoImpl(RabbitTemplate rabbitTemplate, PropriedadeMitigarLance propriedadeMitigarLance) {
        this.rabbitTemplate = rabbitTemplate;
        this.propriedadeMitigarLance = propriedadeMitigarLance;
    }

    @Override
    public void publicarLanceParaMitigacao(Lance lance) {

        log.info("{} Publicando para mitigação ", lance);

        rabbitTemplate.convertAndSend(propriedadeMitigarLance.getTopicExchange(), lance);

    }
}
