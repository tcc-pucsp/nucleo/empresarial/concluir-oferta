package projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.cliente.implementacao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;
import projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.cliente.NotificacaoRecurso;
import projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.modelo.OfertaPublicacao;
import projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.propriedade.PropriedadeConcluirOferta;

@Slf4j
@Component
public class NotificacaoRecursoImpl implements NotificacaoRecurso {

    private final RabbitTemplate rabbitTemplate;

    private final PropriedadeConcluirOferta propriedadeNotificacao;

    public NotificacaoRecursoImpl(RabbitTemplate rabbitTemplate, PropriedadeConcluirOferta propriedadeNotificacao) {
        this.rabbitTemplate = rabbitTemplate;
        this.propriedadeNotificacao = propriedadeNotificacao;
    }

    @Override
    public void notificarConclusao(OfertaPublicacao ofertaPublicacao) {

        log.info("{} publicando para notificacao ", ofertaPublicacao);

        rabbitTemplate.convertAndSend(propriedadeNotificacao.getTopicExchange(), ofertaPublicacao);

    }

}
