package projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.propriedade;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "rabbit.notificacao")
public class PropriedadeConcluirOferta {

    private String topicExchange;

}
