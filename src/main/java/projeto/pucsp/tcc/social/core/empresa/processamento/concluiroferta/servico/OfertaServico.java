package projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.servico;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.cliente.LanceRecurso;
import projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.cliente.NotificacaoRecurso;
import projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.modelo.Lance;
import projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.modelo.OfertaPublicacao;
import projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.recurso.OfertaRecurso;
import projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.repositorio.OfertaRepositorio;

@Slf4j
@Service
public class OfertaServico implements OfertaRecurso {

    private final OfertaRepositorio repositorio;

    private final TransacaoServico transacaoServico;

    private final LanceRecurso lanceRecurso;

    private final NotificacaoRecurso notificacaoRecurso;

    public OfertaServico(
            OfertaRepositorio repositorio,
            TransacaoServico transacaoServico,
            LanceRecurso lanceRecurso,
            NotificacaoRecurso notificacaoRecurso) {
        this.repositorio = repositorio;
        this.transacaoServico = transacaoServico;
        this.lanceRecurso = lanceRecurso;
        this.notificacaoRecurso = notificacaoRecurso;
    }

    @Override
    public void processar(OfertaPublicacao ofertaPublicacao) {

        log.info("Pesquisando por {} ", ofertaPublicacao);

        repositorio
                .findById(ofertaPublicacao.getOfertaId())
                .map(oferta -> oferta.compensar(ofertaPublicacao.getQuantidadeAlimento()))
                .map(oferta -> repositorio.atualizarOferta(oferta.getId(), oferta.getSituacaoOferta(), ofertaPublicacao.getQuantidadeAlimento()))
                .ifPresent(atualizado -> {

                    if (atualizado == 1) {

                        log.info("{} foi compensado com sucesso", ofertaPublicacao);

                        transacaoServico.processar(ofertaPublicacao);

                        notificacaoRecurso.notificarConclusao(ofertaPublicacao);

                    } else if (transacaoServico.obterTransacaoPorOfertaId(ofertaPublicacao.getOfertaId()).isEmpty()) {

                        lanceRecurso.publicarLanceParaMitigacao(new Lance(ofertaPublicacao));

                    }

                });

    }
}
