package projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.servico;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.modelo.OfertaPublicacao;
import projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.modelo.Transacao;
import projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.recurso.OfertaRecurso;
import projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.repositorio.TransacaoRepositorio;

import java.util.Optional;

@Slf4j
@Component
public class TransacaoServico implements OfertaRecurso {

    private final TransacaoRepositorio repositorio;

    public TransacaoServico(TransacaoRepositorio repositorio) {
        this.repositorio = repositorio;
    }

    @Override
    public void processar(OfertaPublicacao ofertaPublicacao) {

        log.info("Comitando transação para {}", ofertaPublicacao);

        repositorio.save(new Transacao(ofertaPublicacao));

    }

    Optional<Transacao> obterTransacaoPorOfertaId(Integer id){
        return repositorio.findById(id);
    }
}
