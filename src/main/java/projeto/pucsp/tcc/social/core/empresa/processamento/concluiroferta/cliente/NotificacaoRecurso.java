package projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.cliente;

import projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.modelo.OfertaPublicacao;

public interface NotificacaoRecurso {

    void notificarConclusao(OfertaPublicacao ofertaPublicacao);

}
