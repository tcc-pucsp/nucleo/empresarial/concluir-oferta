package projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.observador;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.modelo.OfertaPublicacao;
import projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.recurso.OfertaRecurso;

@Component
public class OfertaObservador implements OfertaRecurso {

    private final OfertaRecurso ofertaRecurso;

    public OfertaObservador(@Qualifier("ofertaServico") OfertaRecurso ofertaRecurso) {
        this.ofertaRecurso = ofertaRecurso;
    }

    @Override
    @RabbitListener(queues = "concluir.oferta")
    public void processar(OfertaPublicacao ofertaPublicacao) {

        ofertaRecurso.processar(ofertaPublicacao);
        
    }

}
