package projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.cliente;

import projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.modelo.Lance;

public interface LanceRecurso {

    void publicarLanceParaMitigacao(Lance lance);

}
