package projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.modelo;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "transacao")
@Data
public class Transacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "lance_fk")
    private Integer lanceFk;

    @Column(name = "oferta_fk")
    private Integer ofertaFk;

    @Column(name = "quantidade_alimento")
    private Integer quantidadeAlimento;

    public Transacao(OfertaPublicacao ofertaPublicacao) {
        lanceFk = ofertaPublicacao.getLanceId();
        ofertaFk = ofertaPublicacao.getOfertaId();
        quantidadeAlimento = ofertaPublicacao.getQuantidadeAlimento();
    }

}
