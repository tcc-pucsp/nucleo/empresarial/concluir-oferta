package projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.modelo;

import lombok.Data;

@Data
public class OfertaPublicacao {

    private Integer ofertaId;

    private Integer lanceId;

    private Integer empresarialId;

    private Integer socialId;

    private Integer alimentoId;

    private Integer quantidadeAlimento;

}
