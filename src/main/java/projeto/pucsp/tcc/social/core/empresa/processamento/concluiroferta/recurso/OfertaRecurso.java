package projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.recurso;

import projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.modelo.OfertaPublicacao;

public interface OfertaRecurso {

    void processar(OfertaPublicacao ofertaPublicacao);

}
