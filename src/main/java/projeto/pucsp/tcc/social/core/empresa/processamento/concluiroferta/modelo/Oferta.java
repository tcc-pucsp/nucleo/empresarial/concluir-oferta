package projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.modelo;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "oferta")
@Data
public class Oferta {

    private static final Integer COMPENSAMENTO_POR_COMPLETO = 2;

    private static final Integer COMPENSAMENTO_PARCIAL = 1;

    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "empresa_fk")
    private Integer empresaFk;

    @Column(name = "situacao_oferta")
    private Integer situacaoOferta;

    @Column(name = "quantidade_alimento")
    private Integer quantidadeAlimento;

    @Column(name = "lote_fk")
    private Integer loteFk;

    public Oferta compensar(Integer quantidadeAlimento) {

        if (calcularVariacaoCompensamento(quantidadeAlimento) >= 0) {

            situacaoOferta = COMPENSAMENTO_POR_COMPLETO;

        } else {

            situacaoOferta = COMPENSAMENTO_PARCIAL;

        }

        return this;

    }

    private Integer calcularVariacaoCompensamento(Integer quantidadeAlimento) {
        return quantidadeAlimento - this.quantidadeAlimento;
    }
}
