package projeto.pucsp.tcc.social.core.empresa.processamento.concluiroferta.modelo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.Value;

@Getter
@ToString
@EqualsAndHashCode
@Value
public class Lance {

    private final Integer codigo;

    private final Integer quantidade;

    public Lance(OfertaPublicacao ofertaPublicacao) {
        codigo = ofertaPublicacao.getLanceId();
        quantidade = ofertaPublicacao.getQuantidadeAlimento();
    }
}
